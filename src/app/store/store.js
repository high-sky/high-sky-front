import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger';
import { loadingBarMiddleware } from 'react-redux-loading-bar';
import { composeWithDevTools } from 'redux-devtools-extension';

import reducers from './reducers';

const configureStore = () => {
  const loadingBar = loadingBarMiddleware({
    promiseTypeSuffixes: ['REQUEST', 'SUCCESS', 'FAIL'],
  });

  const logger = createLogger(); // eslint-disable-line no-unused-vars
  const middleware = applyMiddleware(thunk, loadingBar, /* logger */);
  return createStore(reducers, composeWithDevTools(middleware));
};

export default configureStore;
