import { combineReducers } from 'redux';

import { reducer as notificationsReducer } from 'reapop';
import { reducer as formReducer } from 'redux-form';
import { loadingBarReducer } from 'react-redux-loading-bar';

import { reducer as mainComponentReducer } from 'app/layout/MainComponent';
import { reducer as mainFormReducer } from 'app/pages/MainForm';
import { reducer as loaderReducer } from 'app/components/Loader';

const reducers = combineReducers({
  form: formReducer,
  loadingBar: loadingBarReducer,
  notifications: notificationsReducer(),

  data: mainComponentReducer,
  loading: loaderReducer,
  mainForm: mainFormReducer,
});

export default reducers;
