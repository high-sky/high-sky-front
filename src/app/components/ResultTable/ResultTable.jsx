import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Table } from 'reactstrap';
import { isEmpty } from 'lodash/lang';
import { map } from 'lodash/collection';

import { route } from 'app/utils/structure';

class ResultTable extends Component {
  renderRoute = (route, index) => (
    <tr>
      <th scope="row">{ index + 1 }</th>
      <th scope="row">{ route.route }</th>
      <th scope="row">{ route.sum }</th>
    </tr>
  );

  render() {
    const { routes } = this.props;
    if (isEmpty(routes)) {
      return <h3 className="text-center">Список пустой</h3>
    }

    return (
      <Table>
        <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Название рейса</th>
          <th scope="col">Количество посадок</th>
        </tr>
        </thead>
        <tbody>
        { map(routes, (route, i) => this.renderRoute(route, i)) }
        </tbody>
      </Table>
    );
  }
}

ResultTable.propTypes = {
  routes: PropTypes.arrayOf(route)
};

ResultTable.defaultProps = {
  routes: []
};

export default ResultTable;
