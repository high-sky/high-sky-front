import { connect } from 'react-redux';
import { branch, compose, renderComponent } from 'recompose';

import Loader from 'app/components/Loader';
import ResultTable from './ResultTable';

const mapStateToProps = (state, props) => ({
  loading: state.loading.routes,
  routes: props.routes,
});

const renderWhileLoading = (component) =>
  branch(
    props => props.loading,
    renderComponent(component),
  );

export default compose(
  connect(mapStateToProps),
  renderWhileLoading(Loader)
)(ResultTable);
