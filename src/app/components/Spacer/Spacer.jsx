import React, { Component } from 'react';

class Spacer extends Component {
  render() {
    return (
      <div className="mt-2 mb-2">&nbsp;</div>
    );
  }
}

export default Spacer;
