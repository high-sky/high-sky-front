import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Field } from 'redux-form';
import { FormFeedback, Input, Label } from 'reactstrap';
import { map } from 'lodash/collection';

import { item } from 'app/utils/structure';

const Select = ({ id, label, items, input, meta: { touched, error } }) => (
  <div className="form-group">
    <Label htmlFor={ id }>{ label }</Label>
    <Input id={ id } type="select" className="form-control" invalid={!!(touched && error)} { ...input }>
      <option disabled/>
      { map(items, (item, i) =>
        <option key={ i } value={ item.key }>{ item.value }</option>)
      }
    </Input>
    { touched && error && <FormFeedback>{ error }</FormFeedback> }
  </div>
);

const SelectField = ({ name, ...rest }) => <Field component={ Select } name={ name } { ...rest }/>;

SelectField.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  items: PropTypes.arrayOf(item),
  label: PropTypes.string,
};

SelectField.defaultProps = {
  items: [],
  label: ''
};

export default SelectField;
