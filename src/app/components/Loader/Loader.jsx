import React, { Component } from 'react';

import { PulseLoader } from 'halogenium';

class Loader extends Component {
  render() {
    return (
      <div className="container h-100">
        <div className="row align-items-center justify-content-center h-100">
          <PulseLoader size="24px" margin="4px" color="#303680"/>
        </div>
      </div>
    );
  }
}

export default Loader;
