import * as types from './types';

const requestStart = (form) => ({ form, type: types.LOAD_REQUEST });
const requestSuccess = (form) => ({ form, type: types.LOAD_SUCCESS });
const requestFailed = (form) => ({ form, type: types.LOAD_FAIL });

export { requestStart, requestSuccess, requestFailed };
