export const LOAD_REQUEST = 'high-sky/loader/LOAD_REQUEST';
export const LOAD_SUCCESS = 'high-sky/loader/LOAD_SUCCESS';
export const LOAD_FAIL = 'high-sky/loader/LOAD_FAIL';
