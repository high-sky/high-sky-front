import * as actions from './actions';

const requestStart = (form) => actions.requestStart(form);
const requestSuccess = (form) => actions.requestSuccess(form);
const requestFailed = (form) => actions.requestFailed(form);

export { requestStart, requestSuccess, requestFailed };
