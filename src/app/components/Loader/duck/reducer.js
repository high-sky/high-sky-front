import * as types from './types';

const reducer = (state = {}, action) => {
  const form = action.form;
  switch (action.type) {
    case types.LOAD_REQUEST:
      return { ...state, [form]: true };

    case types.LOAD_FAIL:
    case types.LOAD_SUCCESS:
      return { ...state, [form]: false };

    default:
      return state;
  }
};

export default reducer;
