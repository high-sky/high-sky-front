import axios from 'axios';
import { notify } from 'reapop';

import { operations as loaderOperations } from 'app/components/Loader';
import { SubmissionError } from 'redux-form';

const defaultSettings = {
  showProgress: true, // loading bar
  showSuccessMessage: true, // всплывающее сообщение в случае 2xx ответа
  successMessage: 'Данные успешно обновлены на сервере',
  showErrorMessage: true, // всплывающее сообщение в случае 4xx/5xx ответа
  showError: false // ошибка запроса на redux-form
};

const load = (form, url, actions, headers) =>
  makeRequest(form, 'get', url, {}, actions, headers, { showSuccessMessage: false });

const makeRequest = (form, method, url, data, actions, headers, settings) =>
  (dispatch) => {
    const { successType, failType } = actions;
    const { successMessage, showSuccessMessage, showErrorMessage, showError, showProgress } = { ...defaultSettings, ...settings };

    showProgress && dispatch(loaderOperations.requestStart(form));

    method = method || 'post';
    headers = {
      Accept: 'application/json',
      'Content-type': 'application/json; charset=UTF-8',
      ...headers,
    };

    return axios({ url, method, data, headers, })
      .then(response => {
        showProgress && dispatch(loaderOperations.requestSuccess(form));
        showSuccessMessage && dispatch(successNotification(successMessage));

        if (successType) {
          dispatch({ ...successType, payload: response.data });
        }

        return response.data;
      })
      .catch((error) => {
        showProgress && dispatch(loaderOperations.requestFailed(form));
        showErrorMessage && dispatch(errorNotification(error));
        showError && showFormError(error);

        failType && dispatch({ ...failType, error: error });

        throw error;
      });
  };

// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------
const getErrorMessage = (error) =>
  error.response ? error.response.data.message : error.message;

const showFormError = (error) => {
  throw new SubmissionError({ _error: getErrorMessage(error) });
};

// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------
export const successNotification = (message) => notify({
  title: 'Успешно',
  message: message,
  status: 'success',
  dismissible: true,
  dismissAfter: 6000,
});

// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------
export const errorNotification = (error) => {
  const message = getErrorMessage(error);
  console.error('Ошибка', message);
  return notify({
    title: 'Ошибка',
    message: message,
    status: 'error',
    dismissible: true,
    dismissAfter: 6000,
  });
};

export { load, makeRequest };
