import PropTypes from 'prop-types';

export const item = PropTypes.shape({
  key: PropTypes.oneOf([PropTypes.number, PropTypes.string]),
  value: PropTypes.string,
});

export const route = PropTypes.shape({
  route: PropTypes.string,
  sum: PropTypes.number,
});