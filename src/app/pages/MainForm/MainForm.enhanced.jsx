import { connect } from 'react-redux';
import { compose } from 'recompose';
import { reduxForm } from 'redux-form';
import { bindActionCreators } from 'redux';

import MainForm from './MainForm';
import { requestRoutes } from './duck/operations';

const mapStateToProps = (state) => ({
  loading: state.loading.routes,
  airports: state.data.airports,
  routes: state.mainForm.routes,
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators({ requestRoutes }, dispatch);

const formProps = {
  form: 'main-form'
};

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  reduxForm(formProps),
)(MainForm);
