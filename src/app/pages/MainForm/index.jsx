import MainForm from './MainForm.enhanced';
import reducer from './duck';

export { reducer };

export default MainForm;
