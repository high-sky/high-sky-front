import { settings, utils } from 'app/utils';

import * as types from './types';

const requestRoutes = (airport) => {
  const url = `${settings.address}/info?airport=${airport}`;
  const actions = { successType: { type: types.ROUTES_REQUEST_DONE } };
  return utils.makeRequest('routes', 'get', url, {}, actions, {}, { showSuccessMessage: false, showError: true });
};

export { requestRoutes };
