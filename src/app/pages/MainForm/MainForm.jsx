import React, { Component } from 'react';

import { Button } from 'reactstrap';

import Spacer from 'app/components/Spacer';
import SelectField from 'app/components/SelectField';
import ResultTable from 'app/components/ResultTable';
import { required } from 'app/utils/validation';

class MainForm extends Component {
  constructor(props) {
    super(props);
    this.requestRoutes = props.requestRoutes;

    this.state = { requested: false };
  }

  handleSubmit = (values) => {
    this.setState({ requested: false });
    return this.requestRoutes(values.airport)
      .then(() => this.setState({ requested: true }));
  };

  render() {
    const { requested } = this.state;
    const { airports, loading, routes, handleSubmit, error, pristine, submitting } = this.props;
    return (
      <div className="col-lg-6 col-sm-8 border border-secondary rounded">
        <form role="form" className="p-4" onSubmit={ handleSubmit(this.handleSubmit) }>
          <h2>Информация о рейсах</h2>

          <SelectField
            id="airport-select"
            label="Аэропорты"
            items={ airports }
            validate={ required }
            name="airport"/>

          { error && <strong className="text-danger">{ 'Ошибка: ' + error }</strong> }

          { !loading &&
          <div className="text-center">
            <Button color="primary" type="submit" disabled={ pristine || submitting }>Запросить</Button>
          </div>
          }

          { requested && !error &&
          <div>
            <Spacer/>
            <ResultTable routes={ routes }/>
          </div>
          }
        </form>
      </div>
    );
  }
}

export default MainForm;
