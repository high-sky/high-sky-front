import { settings, utils } from 'app/utils';

import * as types from './types';

const loadFormData = () => {
  const url = `${settings.address}/airports`;
  const actions = { successType: { type: types.DATA_LOADED } };
  return utils.load('data', url, actions);
};

const unloadFormData = () => ({ type: types.DATA_UNLOADED });

export { loadFormData, unloadFormData };
