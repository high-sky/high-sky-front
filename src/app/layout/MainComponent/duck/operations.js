import * as actions from './actions';

const loadFormData = actions.loadFormData;
const unloadFormData = actions.unloadFormData;

export { loadFormData, unloadFormData };
