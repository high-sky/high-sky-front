import * as types from './types';

const initialState = {
  airports: [],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case types.DATA_LOADED:
      return { ...state, ...action.payload };

    case types.DATA_UNLOADED:
      return { ...state, ...initialState };

    default:
      return state;
  }
};

export default reducer;
