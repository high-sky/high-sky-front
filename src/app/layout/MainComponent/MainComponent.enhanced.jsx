import { connect } from 'react-redux';
import { branch, compose, lifecycle, renderComponent } from 'recompose';
import { bindActionCreators } from 'redux';

import MainComponent from './MainComponent';
import Loader from 'app/components/Loader';
import { loadFormData, unloadFormData } from './duck/operations';

const mapStateToProps = (state) => ({
  loading: state.loading.data,
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators({ loadFormData, unloadFormData }, dispatch);

const renderWhileLoading = (component) =>
  branch(
    props => props.loading,
    renderComponent(component),
  );

const execAtMount = lifecycle({
  componentWillMount() {
    this.props.loadFormData();
  },

  componentWillUnmount() {
    this.props.unloadFormData();
  },
});

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  execAtMount,
  renderWhileLoading(Loader)
)(MainComponent);
