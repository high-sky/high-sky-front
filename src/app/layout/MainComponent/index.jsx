import MainComponent from './MainComponent.enhanced';
import reducer from './duck';

export { reducer };
export default MainComponent;
