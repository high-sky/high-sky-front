import React, { Component } from 'react';

import MainForm from 'app/pages/MainForm';

class MainComponent extends Component {
  render() {
    return (
      <div className="container h-100">
        <div className="row align-items-center justify-content-center h-100">
          <MainForm/>
        </div>
      </div>
    );
  }
}

export default MainComponent;
