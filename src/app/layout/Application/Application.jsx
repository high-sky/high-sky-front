import React, { Component } from 'react';

import LoadingBar from 'react-redux-loading-bar';
import NotificationsSystem from 'reapop';
import theme from 'reapop-theme-bootstrap';

import MainComponent from 'app/layout/MainComponent';

class Application extends Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  componentDidCatch(error, info) {
    this.setState({ hasError: true, error });
  }

  render() {
    const { error, hasError } = this.state;

    if (hasError) {
      return <h1>{ error.toString() }</h1>;
    }

    return (
      <div className="h-100">
        <LoadingBar/>
        <MainComponent/>
        <NotificationsSystem theme={ theme }/>
      </div>
    );
  }
}

export default Application;
