import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import 'bootstrap/dist/css/bootstrap.css';
import 'font-awesome/css/font-awesome.css';

import Application from 'app/layout/Application';

import registerServiceWorker from './registerServiceWorker';
import configureStore from 'app/store/store';

const RootComponent = () => (
  <Provider store={ configureStore() }>
    <Application/>
  </Provider>
);

ReactDOM.render(<RootComponent/>, document.getElementById('root'));

registerServiceWorker();